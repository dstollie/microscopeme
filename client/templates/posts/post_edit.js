Template.postEdit.events({
	'submit form': function(e) {
		e.preventDefault();

		var postId = this._id;

		var post = {
			url: $(e.target).find('[name=url]').val(),
			title: $(e.target).find('[name=title]').val()
		};

		var errors = validatePost(post);
		if(errors.title || errors.url)
			return Session.set('postEditErrors', errors);

		Posts.update(postId, { $set: post }, function(error) {
			if(error) 
				return Errors.throw(error.reason);

			Router.go('postPage', { _id: postId });
		}); 
	},
	'click .delete': function(e) {
		e.preventDefault();

		if(confirm("Delete this post?")) {
			Posts.remove(this._id);
			Router.go('home')
		}
	}
});

Template.postEdit.created = function() {
	Session.set('postEditErrors', {});
}

Template.postEdit.helpers({
	errorMessage: function(field) {
		return Session.get('postEditErrors')[field];
	},
	errorClass: function(field) {
		return !!Session.get('postEditErrors')[field] ? 'has-error' : '';
	}
});