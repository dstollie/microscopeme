getNotifications =  function() {
    return Notifications.find({userId: Meteor.userId(), read: false});
}

Template.notifications.helpers({
    notifications: function() {
        return getNotifications();
    },
    notificationCount: function() {
        return getNotifications().count();
    }
});

Template.notificationItem.helpers({
   notificationPostPath: function() {
       return Router.routes.postPage.path({_id: this.postId});
   }
});

Template.notificationItem.events({
   'click a': function() {
       Notifications.update(this._id, {$set: {read: true}});
   }
});