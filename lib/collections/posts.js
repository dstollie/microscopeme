Posts = new Mongo.Collection('posts');

Posts.allow({
	update: function(userId, post) {
		return ownsDocument(userId, post);
	},
	remove: function(userId, post) {
		return ownsDocument(userId, post);
	}
});

Posts.deny({
	update: function(userId, post, fieldNames) {
		// may only edit the following field names
		return (_.without(fieldNames, 'url', 'title').length > 0);
	}
});

Posts.deny({
	update: function(userId, post, fieldNames, modifier) {
		var errors =  validatePost(modifier.$set);
		return errors.title || errors.url;
	}
});

Meteor.methods({
	post: function(postAttributes) {
		// Where check here wether the logged in user id is a String
		check(Meteor.userId(), String);
		// Where checkign here whether the submitted data is in Strings
		check(postAttributes, {
			title: String,
			url: String
		});

	    var errors = validatePost(postAttributes);    if (errors.title || errors.url)      
	    	throw new Meteor.Error('invalid-post', "You must set a title and URL for your post");

		// Searching if there is already a post with the same url
		var existingPost = Posts.findOne({ url: postAttributes.url });
		if(existingPost) {
			return {
				postExists: true,
				_id: existingPost._id
			}
		}

		// Getting the loggedin user
		var user = Meteor.user();
		var post = _.extend(postAttributes, {
			userId: user._id,
			author: user.username,
			dateSubmitted: new Date(),
            commentsCount: 0,
            upvoters: [],
            votes: 0
		});

		// Insert the post!
		var postId = Posts.insert(post);

		return {
			_id: postId
		}
	},
    upvote: function(id) {
        check(this.userId, String);
        check(id, String);

        var affected = Posts.update({
            _id: id,
            upvoters: {$ne: this.userId}
        }, {
            $addToSet: {upvoters: this.userId},
            $inc: {votes: 1}
        });

        if(!affected)
            throw new Meteor.Error('invalid', "You weren't able to upvote that post");
    }
});

validatePost = function(post) {
	var errors = {};
	if(!post.title)
		errors.title = "Please fill a headline";

	if(!post.url)
		errors.url = "Please fill in an URL";

	return errors;
}